package org.neo4j.examples;

import java.util.ArrayList;

import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.graphdb.traversal.Uniqueness;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.neo4j.kernel.Traversal;
//import org.neo4j.kernel.Uniqueness;

import static org.neo4j.graphdb.DynamicRelationshipType.withName;

public class TraversalOrderedPath {
	
	// Walking an ordered path.
	// An example showing hoe to use a path context holding a representation of a path.
	
	private static final RelationshipType REL1 = withName("REL1"), REL2 = withName("REL2"), REL3 = withName("REL3");
	private static final String DB_PATH = "data/traversalorderedpath";
	GraphDatabaseService graphDb;

	public TraversalOrderedPath (GraphDatabaseService graphDb) {
		this.graphDb = graphDb;
	}
	
	public static void main (String[] args) {
		GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		TraversalOrderedPath op = new TraversalOrderedPath(graphDb);
		op.shutdownGraph();
	}
	
	public Node createTheGraph() {
		try (Transaction tx = graphDb.beginTx()) {
			Node A = graphDb.createNode();
			Node B = graphDb.createNode();
			Node C = graphDb.createNode();
			Node D = graphDb.createNode();
			
			A.createRelationshipTo(C, REL2);
			C.createRelationshipTo(D, REL3);
			A.createRelationshipTo(B, REL1);
			B.createRelationshipTo(C, REL2);
			
			A.setProperty("name", "A");
			B.setProperty("name", "B");
			C.setProperty("name", "C");
			D.setProperty("name", "D");
			tx.success();
			return A;			
		}
	}
	
	public TraversalDescription findPaths() {
		// Define a path context, i.e. how to walk a path.
		// Store the order of relationships (REL1 -> REL2 -> REL3) in an array. Upon traversal, 
		// the Evaluator can check against it to ensure that only paths are included and returned 
		// that have the predefined order of relationships.
		final ArrayList<RelationshipType> orderedPathContext = new ArrayList<RelationshipType>();
		orderedPathContext.add(REL1);
		orderedPathContext.add(withName("REL2"));
		orderedPathContext.add(withName("REL3"));
		TraversalDescription td = graphDb.traversalDescription()
				.evaluator(new Evaluator() {
					@Override
					public Evaluation evaluate(final Path path) {
						if (path.length() == 0) {
							return Evaluation.EXCLUDE_AND_CONTINUE;
						}
						RelationshipType expectedType = orderedPathContext.get(path.length() - 1);
						boolean isExpectedType = path.lastRelationship().isType(expectedType);
						boolean included = path.length() == orderedPathContext.size() && isExpectedType;
						boolean continued = path.length() < orderedPathContext.size() && isExpectedType;
						return Evaluation.of(included, continued);
					}
				})
				// Set the uniqueness to Uniqueness.NODE_PATH as we want to be able to revisit 
				// the same node during the traversal, but not the same path.
				.uniqueness(Uniqueness.NODE_PATH);
		return td;
	}
	
	String printPaths(TraversalDescription td, Node A) {
		try (Transaction tx = graphDb.beginTx()) {
			String output = "";
			Traverser traverser = td.traverse(A);
			PathPrinter pathPrinter = new PathPrinter("name");
			for (Path path: traverser) {
				output += Traversal.pathToString(path, pathPrinter);
			}
			output += "\n";
			return output;
		}
	}
	
	static class PathPrinter implements Traversal.PathDescriptor<Path> {
		public final String nodePropertyKey;
		public PathPrinter(String nodePropertyKey) {
			this.nodePropertyKey = nodePropertyKey;
		}
		@Override
		public String nodeRepresentation (Path path, Node node) {
			return "(" + node.getProperty(nodePropertyKey, "") + ")";
		}
		@Override
		public String relationshipRepresentation (Path path, Node from, Relationship relationship) {
			String prefix = "--", suffix = "--";
			if (from.equals(relationship.getEndNode())) {
				prefix = "<--";
			} else {
				suffix = "-->";
			}
			return prefix + "[" + relationship.getType().name() + "]" + suffix;
		}
	}
	
	public void shutdownGraph() {
		try {
			if (graphDb != null) {
				graphDb.shutdown();
			}
		} finally {
			graphDb = null;
		}
	}
	
}
