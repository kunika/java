package org.neo4j.examples;

import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.Node;

public class Indexing {
	
	private static final String DB_PATH = "data/indexing";
	private static final String USERNAME_KEY = "username";
	private static GraphDatabaseService graphDb;
	private static Index<Node> nodeIndex;
	
	public static void main(final String[] args) {
		
		// start database
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		registerShutdownHook();
		
		try (Transaction tx = graphDb.beginTx()) {

			// add users
			nodeIndex = graphDb.index().forNodes("nodes");
			for (int id = 0; id < 100; id++) {
				createAndIndexUser(idToUserName(id));
			}
			
			// find user
			// retrieve users by name using the legacy indexing system
			int idToFind = 45;
			String userName = idToUserName(idToFind);
			Node foundUser = nodeIndex.get(USERNAME_KEY, userName).getSingle();
			
			// print 
			System.out.println("The username of user " + idToFind + " is" + foundUser.getProperty(USERNAME_KEY));
			
			// delete the users and remove them from the index
			for (Node user: nodeIndex.query(USERNAME_KEY, "*")) {
				nodeIndex.remove(user, USERNAME_KEY, user.getProperty(USERNAME_KEY));
				user.delete();
			}
			
			tx.success();
		}
		shutdown();
		
	}
	
	private static String idToUserName (final int id) {
		return "user" + id + "@neo4j.org";
	}
	
	private static Node createAndIndexUser (final String username) {
		Node node = graphDb.createNode();
		node.setProperty(USERNAME_KEY, username);
		nodeIndex.add(node,  USERNAME_KEY, username);
		return node;
	}
	
	private static void shutdown() {
		graphDb.shutdown();
	}
	
	private static void registerShutdownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				shutdown();
			}
		});
	}

}
