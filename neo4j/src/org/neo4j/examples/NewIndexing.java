package org.neo4j.examples;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.neo4j.graphdb.schema.Schema;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.ResourceIterator;

public class NewIndexing {
	
	private static final String DB_PATH = "data/newindexing";

	public static void main(final String[] args) {
		
		// start database
		System.out.println("Starting database...");
		GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		
		// create index
		// configure the database to index users by name;
		{
			IndexDefinition indexDefinition;
			try (Transaction tx = graphDb.beginTx()) {
				Schema schema = graphDb.schema();
				indexDefinition = schema.indexFor(DynamicLabel.label("User"))
						.on("username")
						.create();
				tx.success();				
			}
			// wait...
			// indexes are populated asynchronously when first created, and it is possible to use to the core API to wait for index population to complete.
			try (Transaction tx = graphDb.beginTx()) {
				Schema schema = graphDb.schema();
				schema.awaitIndexOnline(indexDefinition, 10, TimeUnit.SECONDS);
			}
		}
		
		// add users
		{
			try (Transaction tx = graphDb.beginTx()) {
				Label label = DynamicLabel.label("User");
				for (int id = 0; id < 100; id++) {
					Node userNode = graphDb.createNode(label);
					userNode.setProperty("username", "user" + id + "@neo4j.org");
				}
				System.out.println("User created");
				tx.success();
			}
		}
		
		// find users
		{
			Label label = DynamicLabel.label("User");
			int idToFind = 45;
			String nameToFind = "user" + idToFind + "@neo4j.org";
			try (Transaction tx = graphDb.beginTx()) {
				try (ResourceIterator<Node> users = graphDb.findNodesByLabelAndProperty(label, "username", nameToFind).iterator()) {
					ArrayList<Node> userNodes = new ArrayList<>();
					while (users.hasNext()) {
						userNodes.add(users.next());
					}
					for (Node node: userNodes) {
						System.out.println("The username of user " + idToFind + " is " + node.getProperty("username"));
					}
				}
			}
		}
		
		// resource iterator
		{
			Label label = DynamicLabel.label("User");
			int idToFind = 45;
			String nameToFind = "user" + idToFind + "@neo4j.org";
			try (Transaction tx = graphDb.beginTx(); ResourceIterator<Node> users = graphDb.findNodesByLabelAndProperty(label, "username", nameToFind).iterator()) {
				Node firstUserNode;
				if (users.hasNext()) {
					firstUserNode = users.next();
				}
				users.close();
			}
		}
		
		// update users
		// when updating the name of a user, the index is updated as well.
		{
			try (Transaction tx = graphDb.beginTx()) {
				Label label = DynamicLabel.label("User");
				int idToFind = 45;
				String nameToFind = "user" + idToFind + "@neo4j.org";
				for (Node node: graphDb.findNodesByLabelAndProperty(label, "username", nameToFind)) {
					node.setProperty("username", "user" + (idToFind + 1) + "@neo4j.org");
				}
				tx.success();
			}
		}
		
		// delete users
		// when deleting a user, it is automatically removed from the index.
		{
			try (Transaction tx = graphDb.beginTx()) {
				Label label = DynamicLabel.label("User");
				int idToFind = 46;
				String nameToFind = "user" + idToFind + "@neo4j.org";
				for (Node node : graphDb.findNodesByLabelAndProperty(label, "username", nameToFind)) {
					node.delete();
				}
				tx.success();
			}
		}
		
		// drop index
		// in case we change our data model, we can drop the index as well.
		try (Transaction tx = graphDb.beginTx()) {
			Label label = DynamicLabel.label("User");
			for (IndexDefinition indexDefinition : graphDb.schema().getIndexes(label)) {
				indexDefinition.drop();
			}
			tx.success();
		}
		
		// shutdown database
		System.out.println("Sutting down database....");
		graphDb.shutdown();
	}

}
