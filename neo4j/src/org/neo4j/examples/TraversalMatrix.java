package org.neo4j.examples;

import java.io.File;

import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;

public class TraversalMatrix {
	
	public enum RelTypes implements RelationshipType {
		NEO_NODE,
		KNOWS,
		CODED_BY
	}
	
	private static final String DB_PATH = "data/traversalmatrix";
	private GraphDatabaseService graphDb;
	private long matrixNodeId;
	
	public static void main(String[] args) {
		TraversalMatrix matrix = new TraversalMatrix();
		matrix.setUp();
		System.out.println(matrix.printNeoFriends());
		System.out.println(matrix.printMatrixHackers());
		matrix.shutdown();
	}
	
	public void setUp() {
		deleteFileOrDirectory(new File(DB_PATH));
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		registerShutdownHook();
		createNodespace();
	}
	
	public void createNodespace () {
		
		try (Transaction tx = graphDb.beginTx()) {
			// create matrix node
			Node matrix = graphDb.createNode();
			matrixNodeId = matrix.getId();
			
			// create neo
			Node thomas = graphDb.createNode();
			thomas.setProperty("name", "Thomas Anderson");
			thomas.setProperty("age", 29);
			
			// connect neo/thomas to the matrix node
			matrix.createRelationshipTo(thomas, RelTypes.NEO_NODE);
			
			Node trinity = graphDb.createNode();
			trinity.setProperty("name", "Trinity");
			Relationship rel = thomas.createRelationshipTo(trinity, RelTypes.KNOWS);
			rel.setProperty("age", "3 days");
			
			Node morpheus = graphDb.createNode();
			morpheus.setProperty("name", "Morpheus");
			morpheus.setProperty("rank", "Captain");
			morpheus.setProperty("occupation", "Total badass");
			thomas.createRelationshipTo(morpheus, RelTypes.KNOWS);
			rel = morpheus.createRelationshipTo(trinity, RelTypes.KNOWS);
			rel.setProperty("age", "12 years");
			
			Node cypher = graphDb.createNode();
			cypher.setProperty("name", "Cypher");
			cypher.setProperty("last name", "Reagan");
			trinity.createRelationshipTo(cypher, RelTypes.KNOWS);
			rel = morpheus.createRelationshipTo(cypher, RelTypes.KNOWS);
			rel.setProperty("disclosure", "public");
			
			Node smith = graphDb.createNode();
			smith.setProperty("name", "Agent Smith");
			smith.setProperty("version", "1.0b");
			smith.setProperty("language", "C++");
			rel = cypher.createRelationshipTo(smith, RelTypes.KNOWS);
			rel.setProperty("disclosure", "secret");
			rel.setProperty("age", "6 months");
			
			Node architect = graphDb.createNode();
			architect.setProperty("name", "The Architect");
			smith.createRelationshipTo(architect, RelTypes.CODED_BY);
			
			tx.success();
		}
	}
	
	private Node getNeoNode() {
		return graphDb.getNodeById(matrixNodeId)
				.getSingleRelationship(RelTypes.NEO_NODE, Direction.OUTGOING)
				.getEndNode();
	}
	
	public String printNeoFriends() {
		try (Transaction tx = graphDb.beginTx()) {
			
			// get neo
			Node neoNode = getNeoNode();
			
			// friends
			int numberOfFriends = 0;
			String output = neoNode.getProperty("name") + "'s friends:\n";
			Traverser friendsTraverser = getFriends(neoNode);
			for (Path friendPath: friendsTraverser) {
				output += "At depth " + friendPath.length() + " => " 
						+ friendPath.endNode().getProperty("name") + "\n";
				numberOfFriends++;
			}
			output += "Number of friends found: " + numberOfFriends + "\n";
			return output;
		}
	}
	
	private Traverser getFriends(final Node person) {
		TraversalDescription td = graphDb.traversalDescription()
			.breadthFirst()
			.relationships(RelTypes.KNOWS, Direction.OUTGOING)
			.evaluator(Evaluators.excludeStartPosition());
		return td.traverse(person);
	}
	
	public String printMatrixHackers() {
		try (Transaction tx = graphDb.beginTx()) {
			String output = "Hackers:\n";
			int numberOfHackers = 0;
			Traverser traverser = findHackers(getNeoNode());
			for (Path hackerPath: traverser) {
				output += "At depth " + hackerPath.length() + " => "
						+ hackerPath.endNode().getProperty("name") + "\n";
				numberOfHackers++;
			}
			output += "Number of hackers found: " + numberOfHackers + "\n";
			return output;
		}
	}
	
	private Traverser findHackers(final Node startNode) {
		TraversalDescription td = graphDb.traversalDescription()
			.breadthFirst()
			.relationships(RelTypes.CODED_BY, Direction.OUTGOING)
			.relationships(RelTypes.KNOWS, Direction.OUTGOING)
			.evaluator(Evaluators.includeWhereLastRelationshipTypeIs( RelTypes.CODED_BY ));
		return td.traverse( startNode );
	}
	
	public void shutdown() {
		graphDb.shutdown();
	}
	
	private void registerShutdownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}
	
	private static void deleteFileOrDirectory(final File file) {
		if (!file.exists()) {
			return;
		}
		if (file.isDirectory()) {
			for (File child: file.listFiles()) {
				deleteFileOrDirectory(child);
			}
		} else {
			file.delete();
		}		
	}

}
