package org.neo4j.examples;

import java.io.IOException;

import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Direction;

import java.io.File;
import org.neo4j.kernel.impl.util.FileUtils;


public class HelloWorld {
	
	private static final String DB_PATH = "data/hello";
	
	public String greeting;
	
	GraphDatabaseService graphDb;
	Node firstNode;
	Node secondNode;
	Relationship relationship;
	
	private static enum RelTypes implements RelationshipType {
		KNOWS
	}
	
	public static void main(final String[] args) {
		HelloWorld hello = new HelloWorld();
		hello.createDb();
		hello.removeData();
		hello.shutDown();		
	}
	
	private void clearDb() {
		try {
			FileUtils.deleteRecursively(new File(DB_PATH));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void createDb() {
		
		// clear database
		clearDb();
		
		// start database
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		
		registerShutdownHook(graphDb);
		// create nodes and relationship
		try (Transaction tx = graphDb.beginTx()) {
			firstNode = graphDb.createNode();
			firstNode.setProperty("message", "Hello, ");
			secondNode = graphDb.createNode();
			secondNode.setProperty("message", "World!");
			relationship = firstNode.createRelationshipTo(secondNode, RelTypes.KNOWS);
			relationship.setProperty("message", "brave neo4j ");
			System.out.println(firstNode.getProperty("message"));
			System.out.println(relationship.getProperty("message"));
			System.out.println(secondNode.getProperty("message"));
			greeting = ((String) firstNode.getProperty("message"))
					+ ((String) relationship.getProperty("message"))
					+ ((String) secondNode.getProperty("message"));
			
			tx.success();
		}
	}
	
	public void removeData() {
		try (Transaction tx = graphDb.beginTx()) {
			firstNode.getSingleRelationship(RelTypes.KNOWS, Direction.OUTGOING).delete();
			firstNode.delete();
			secondNode.delete();
			tx.success();
		}
	}
	
	public void shutDown() {
		System.out.println();
		System.out.println("Shutting down database...");
		graphDb.shutdown();
	}
	
	private static void registerShutdownHook (final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}
	
}
