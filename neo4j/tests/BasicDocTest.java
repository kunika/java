package org.neo4j.tests;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.test.TestGraphDatabaseFactory;

public class BasicDocTest {

	public static void main(String[] args) {
		
		protected GraphDatabaseService graphDb;
		
		@Before
		public void prepareTestDatabase() {
			graphDb = new TestGraphDatabaseFactory.newImpermanentDatabase();
		}
		
		@After
		public void destroyTestDatabase() {
			graphDb.shutdown();
		}
		
		@Test
		public vid startWithConfiguration() {
			GraphDatabaseService db = newTestGraphDatabaseFactory()
					.newImpermanentDatabaseBuilder()
					.setConfig(GraphDatabaseSettings.nodestore_mapped_memory_size, "10M")
					.setConfig(GraphDatabaseSettings.string_block_size, "60")
					.setConfig(GraphDatabaseSettings.array_block_size, "300")
					.newGraphDatabase();
			db.shutdown();
		}
		
		@Test
		public void shouldCreateNode() {
			Node n = null;
			try (Transaction tx = graphDb.beginTx()) {
				n = graphDb.createNode();
				n.setProperty("name", "Nancy");
				tx.success();
			}
			
			assertThat(n.getId(), is(greaterThan(-1L)));
			
			try (Transaction tx = graphDb.beginTx()) {
				Node foundNode = graphDb.getNodeById(n.getId());
				assertThat(foundNode.getId(), is(n.getId()));
				assertThat((String) foundNode.getProperty("name", is("Nancy")));
			}
		}

	}

}
